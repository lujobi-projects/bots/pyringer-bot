# This example requires the 'message_content' intent.

import os
import discord
import re

intents = discord.Intents.default()
intents.message_content = True

client = discord.Client(intents=intents)

regex = r"\@p\w*ringer"


@client.event
async def on_ready():
    print(f"We have logged in as {client.user}")


@client.event
async def on_message(message):
    if message.author == client.user:
        return

    print(message.content + " " + str(message.author) + " " + str(message.author.id))

    try:
        print(
            message.content
            + " "
            + str(message.author)
            + " "
            + str(message.author.id)
            + " "
            + str(message.author.mention)
        )
    except:
        pass

    if len(re.findall(regex, message.content, re.MULTILINE)) > 0:
        await message.channel.send("@Piri")


client.run(os.getenv("TOKEN"))
